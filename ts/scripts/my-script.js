//my-script.js
const myButton = document.querySelector('#my-button')

function obtainWindow(name) {
   return new Promise((resolve, reject) => {
     overwolf.windows.obtainDeclaredWindow(name, (response) => {
       if (response.status !== "success") {
         return reject(response);
       }
 
       resolve(response);
     });
   });
 }
 
 function restore(name) {
   return new Promise(async (resolve, reject) => {
     try {
       await obtainWindow(name);
       overwolf.windows.restore(name, (result) => {
         if (result.status === "success") {
           resolve(result);
         } else {
           reject(result);
         }
       });
     } catch (e) {
       reject(e);
     }
   });
 }

myButton.addEventListener('click', async event => {
   event.preventDefault();
   console.log('open another window click')
   try {
       const windowOpenedResult = await restore('OVERLAY-WINDOW-NAME')
       console.log(windowOpenedResult) // done
       
   } catch (error) {
       console.log(error) //error
   }
 });